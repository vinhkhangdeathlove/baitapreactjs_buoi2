import "./App.css";
import ChangeGlasses from "./Component/ChangeGlasses";

function App() {
  return (
    <div className="App">
      <ChangeGlasses />
    </div>
  );
}

export default App;
