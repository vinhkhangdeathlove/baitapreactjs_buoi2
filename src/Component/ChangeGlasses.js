import React, { Component } from "react";
import { glassesData } from "./dataGlasses";
import GlassList from "./GlassList";
import Model from "./Model";

export default class ChangeGlasses extends Component {
  state = {
    glassList: glassesData,
    glassItem: {},
    showDetail: false,
  };

  handleChangeGlass = (id) => {
    let index = this.state.glassList.findIndex((item) => {
      return item.id === id;
    });
    this.setState({ glassItem: this.state.glassList[index] });
    this.setState({ showDetail: true });
  };

  render() {
    return (
      <div className="container mt-5">
        <div className="row justify-content-between">
          <GlassList
            glassList={this.state.glassList}
            handleChangeGlass={this.handleChangeGlass}
          />
          <Model
            glassItem={this.state.glassItem}
            showDetail={this.state.showDetail}
          />
        </div>
      </div>
    );
  }
}
