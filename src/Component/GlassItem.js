import React, { Component } from "react";

export default class GlassItem extends Component {
  render() {
    let { id, url } = this.props.data;

    return (
      <div className="col-4">
        <label>
          <img src={url} className="glasses" />
          <button
            className="btn btn-primary"
            onClick={() => {
              this.props.handleChangeGlass(id);
            }}
          ></button>
        </label>
      </div>
    );
  }
}
