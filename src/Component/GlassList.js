import React, { Component } from "react";
import GlassItem from "./GlassItem";

export default class GlassList extends Component {
  render() {
    return (
      <div className="col-6 vglasses__left">
        <h1>Virtual Glasses</h1>
        <div className="row" id="vglassesList">
          {this.props.glassList.map((item, index) => {
            return (
              <GlassItem
                data={item}
                key={index}
                handleChangeGlass={this.props.handleChangeGlass}
              />
            );
          })}
        </div>
      </div>
    );
  }
}
