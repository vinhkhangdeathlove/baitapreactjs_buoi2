import React, { Component } from "react";

export default class Model extends Component {
  render() {
    let { url, name, price, desc } = this.props.glassItem;

    return (
      <div className="col-4 vglasses__right">
        <div className="vglasses__card">
          <div className="vglasses__model" id="avatar">
            <div id="avatarHasGlass">
              <img
                src={url}
                style={{ display: this.props.showDetail ? "block" : "none" }}
              />
            </div>
            <div
              id="glassesInfo"
              className="vglasses__info"
              style={{ display: this.props.showDetail ? "block" : "none" }}
            >
              <h4>{name}</h4>
              <div className="price d-flex">
                <p>
                  $<span>{price}</span>
                </p>
                <span className="stock">Stocking</span>
              </div>
              <div className="describe">
                <p>{desc}</p>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
